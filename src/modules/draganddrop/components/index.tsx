import * as React from "react";
import Game from "./Game";

export default () => (
  <div>
    <h1>Drag and Drop Example</h1>
    <p>
      Basically the tutoral from{" "}
      <a href="http://react-dnd.github.io/react-dnd/docs-tutorial.html">here</a>.
    </p>
    <Game />
  </div>
);
