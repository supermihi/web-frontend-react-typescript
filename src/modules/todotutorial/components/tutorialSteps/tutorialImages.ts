import todoState from "./img/todoState.png";
import todoItem from "./img/todoItem.png";
import todo_mockup from "./img/todo_mockup.png";
import react_logo from "./img/react.png";
import redux_logo from "./img/redux.png";
import todoAdd_interactions from "./img/todoAdd_interactions.png";

const images: { [name: string]: string } = {
  todoState,
  todoItem,
  todoAdd_interactions,
  react_logo,
  redux_logo,
  todo_mockup
};
export default images;
