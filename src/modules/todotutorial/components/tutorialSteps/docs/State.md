# Hello State!

It goes without saying, that a copy-paste approach will save you many typos! So whenever I write
"add a file ...", you should rather copy-paste the boilerplate file and adapt it to your needs.

Go ahead and start your own module by creating a new folder (e.g. `mytodos`) in `modules`.
Peek into the existing `todo` module to see how things are done. Create the
file `model.ts` and there write out the interfaces for `TodoItem` and `TodoState`,

![todoItem](todoItem)

![todoState](todoState)
