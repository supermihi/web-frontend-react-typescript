# Welcome!

So you want to get into web frontend development with our React/Redux boilerplate? Great! We'll
dive right in with a todo app – the most popular choice for tutorials for any web design framework.

This is what we will do:

![mockup](todo_mockup)

There are todo items in a list, they can be checked (i.e. completed) or not, they can be removed, and
new todos can be added to the list.

There is already a [reference implementation of the "Enter new Todo" part](/todos), in the
`modules/todo` directory. When you go through all the steps later on, you can always peek there how to get it done.

Since this boilerplate is all about React and Redux (click on the logos below to get more info), we will
do the html components using React, and we will use the Redux framework to handle the "data", the state of
the application (the todo list and a little more...).

[![react_logo](react_logo)](https://reactjs.org/)
[![redux_logo](redux_logo)](http://redux.js.org/)

But wait. Maybe it's a good idea to hit the web and look for some typescript tutorials first. We are using
a new language to do this, so prepare for the usual frustration when learning a new programming language.

Want to get a flavor for Redux in particular, or just get into the javascript spirit? Watch
these [free video lessons](https://egghead.io/courses/getting-started-with-redux). **Highly recommended!**

Ready to go? Hit the "Next" button below to dive in!
