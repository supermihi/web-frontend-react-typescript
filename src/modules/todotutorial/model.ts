export interface TodoTutorialState {
  currentStep: number;
  totalNumSteps: number;
}
