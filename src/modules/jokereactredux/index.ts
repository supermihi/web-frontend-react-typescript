import Component from "./components";
import reducer from "./reducer";
import { NAME, GlobalState } from "./global";
import { ReactReduxJokeState as State } from "./model";

export { Component, reducer, GlobalState, NAME, State };
