import reducer from "./reducers";
import Component from "./components";
import { NAME, GlobalState } from "./global";
import { TodoState as State } from "./model";
export { NAME, GlobalState, reducer, Component, State };
